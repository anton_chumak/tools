import 'dart:async';

import 'package:flutter_test/flutter_test.dart';
import 'package:mockito/annotations.dart';
import 'package:mockito/mockito.dart';
import 'package:quadrate_tools/service/dispose_helper.dart';
import 'package:quadrate_tools/service/property_controller.dart';
import 'package:rxdart/rxdart.dart';

import 'dispose_helper_test.mocks.dart';

// flutter pub run build_runner build --delete-conflicting-outputs

class DisposableObjectNoDispose with DisposeHelper {}

class DisposableObjectWithDispose with DisposeHelper {
  void dispose() {
    super.dispose();
  }
}

String valueGetter() {
  return 'value';
}

// flutter packages pub run build_runner build lib
@GenerateMocks([StreamSubscription, Subject],
    customMocks: [
    MockSpec<PropertyController>(
        fallbackGenerators:  <Symbol, Function>{#value: valueGetter}),
    ])
void main() {
  test('Dispose', () async {
    final disposableObjectNoDispose = DisposableObjectNoDispose();

    final subject = MockSubject();
    when(subject.close())
        .thenAnswer((realInvocation) => Future.value());
    final subscription = MockStreamSubscription();
    final propertyController = MockPropertyController();

    disposableObjectNoDispose.addSubject(subject);
    disposableObjectNoDispose.addSubscription(subscription);
    disposableObjectNoDispose.addController(propertyController);
    disposableObjectNoDispose.dispose();
    verify(subject.close()).called(1);
    verify(subscription.cancel()).called(1);
    verify(propertyController.dispose()).called(1);
  });

  test('Dispose override', () async {
    final disposableObjectNoDispose = DisposableObjectWithDispose();

    final subject = MockSubject();
    when(subject.close())
        .thenAnswer((realInvocation) => Future.value());
    final subscription = MockStreamSubscription();
    final propertyController = MockPropertyController();

    disposableObjectNoDispose.addSubject(subject);
    disposableObjectNoDispose.addSubscription(subscription);
    disposableObjectNoDispose.addController(propertyController);
    disposableObjectNoDispose.dispose();
    verify(subject.close()).called(1);
    verify(subscription.cancel()).called(1);
    verify(propertyController.dispose()).called(1);
  });
}
