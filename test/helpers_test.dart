import 'package:flutter/material.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:quadrate_tools/service/helpers.dart';

void main() {

  test('divide tiles (three items)', () async {
    Widget widget1 = Text('one');
    Widget widget2 = Text('two');
    Widget widget3 = Text('three');
    Widget divider = Divider();
    final result =
    divideTiles(divider: divider, tiles: [widget1, widget2, widget3]);
    expect(result, [widget1, divider, widget2, divider, widget3]);
  });

  test('divide tiles (two items)', () async {
    Widget widget1 = Text('one');
    Widget widget2 = Text('two');
    Widget divider = Divider();
    final result = divideTiles(divider: divider, tiles: [widget1, widget2]);
    expect(result, [widget1, divider, widget2]);
  });

  test('divide tiles (one item)', () async {
    Widget widget1 = Text('one');
    Widget divider = Divider();
    final result = divideTiles(divider: divider, tiles: [widget1]);
    expect(result, [widget1]);
  });

  test('divide tiles (empty iterator)', () async {
    Widget divider = Divider();
    final result = divideTiles(divider: divider, tiles: []);
    expect(result, []);
  });

  test('isDateRangesOverlap', () async {
    DateRange getRange(int start, int? end) =>
        DateRange(start: DateTime(2023, 09, start), end: end  == null ? null: DateTime(2023, 09, end));

    // After,
    expect(isDateRangesOverlap(getRange(1, 2), getRange(3, 4)), false);
    expect(isDateRangesOverlap(getRange(1, 2), getRange(3, null)), false);
    // StartTouching,
    expect(isDateRangesOverlap(getRange(1, 2), getRange(2, 4)), false);
    expect(isDateRangesOverlap(getRange(1, 2), getRange(2, null)), false);
    // StartInside,
    expect(isDateRangesOverlap(getRange(1, 3), getRange(2, 4)), true);
    expect(isDateRangesOverlap(getRange(1, 3), getRange(2, null)), true);
    // InsideStartTouching,
    expect(isDateRangesOverlap(getRange(1, 4), getRange(1, 3)), true);
    expect(isDateRangesOverlap(getRange(1, 4), getRange(1, null)), false);
    // EnclosingStartTouching,
    expect(isDateRangesOverlap(getRange(1, 2), getRange(1, 3)), true);
    // Enclosing,
    expect(isDateRangesOverlap(getRange(2, 3), getRange(1, 4)), true);
    expect(isDateRangesOverlap(getRange(2, 3), getRange(1, null)), false);
    // EnclosingEndTouching,
    expect(isDateRangesOverlap(getRange(2, 4), getRange(1, 4)), true);
    // ExactMatch,
    expect(isDateRangesOverlap(getRange(1, 4), getRange(1, 4)), true);
    // Inside,
    expect(isDateRangesOverlap(getRange(1, 4), getRange(2, 3)), true);
    // InsideEndTouching,
    expect(isDateRangesOverlap(getRange(1, 4), getRange(2, 4)), true);
    expect(isDateRangesOverlap(getRange(1, 4), getRange(4, null)), false);
    // EndInside,
    expect(isDateRangesOverlap(getRange(2, 5), getRange(1, 4)), true);
    expect(isDateRangesOverlap(getRange(2, 5), getRange(4, 1)), true);
    expect(isDateRangesOverlap(getRange(5, 2), getRange(1, 4)), true);
    expect(isDateRangesOverlap(getRange(5, 2), getRange(4, 1)), true);
    // EndTouching,
    expect(isDateRangesOverlap(getRange(2, 3), getRange(1, 2)), false);
    // Before,
    expect(isDateRangesOverlap(getRange(3, 4), getRange(1, 2)), false);
    expect(isDateRangesOverlap(getRange(4, 3), getRange(1, 2)), false);
    expect(isDateRangesOverlap(getRange(3, 4), getRange(2, 1)), false);
    expect(isDateRangesOverlap(getRange(4, 3), getRange(2, 1)), false);
    expect(isDateRangesOverlap(getRange(4, 3), getRange(2, null)), false);
  });

  test('isDateMillisRangesOverlap', () async {
    DateMillisRange getRange(int start, int? end) => DateMillisRange(
        start: DateTime(2023, 09, start).millisecondsSinceEpoch,
        end: end == null ? null : DateTime(2023, 09, end).millisecondsSinceEpoch);

    // After,
    expect(isDateMillisRangesOverlap(getRange(1, 2), getRange(3, 4)), false);
    expect(isDateMillisRangesOverlap(getRange(1, 2), getRange(3, null)), false);
    // StartTouching,
    expect(isDateMillisRangesOverlap(getRange(1, 2), getRange(2, 4)), false);
    expect(isDateMillisRangesOverlap(getRange(1, 2), getRange(2, null)), false);
    // StartInside,
    expect(isDateMillisRangesOverlap(getRange(1, 3), getRange(2, 4)), true);
    expect(isDateMillisRangesOverlap(getRange(1, 3), getRange(2, null)), true);
    // InsideStartTouching,
    expect(isDateMillisRangesOverlap(getRange(1, 4), getRange(1, 3)), true);
    expect(isDateMillisRangesOverlap(getRange(1, 4), getRange(1, null)), false);
    // EnclosingStartTouching,
    expect(isDateMillisRangesOverlap(getRange(1, 2), getRange(1, 3)), true);
    // Enclosing,
    expect(isDateMillisRangesOverlap(getRange(2, 3), getRange(1, 4)), true);
    expect(isDateMillisRangesOverlap(getRange(2, 3), getRange(1, null)), false);
    // EnclosingEndTouching,
    expect(isDateMillisRangesOverlap(getRange(2, 4), getRange(1, 4)), true);
    // ExactMatch,
    expect(isDateMillisRangesOverlap(getRange(1, 4), getRange(1, 4)), true);
    // Inside,
    expect(isDateMillisRangesOverlap(getRange(1, 4), getRange(2, 3)), true);
    // InsideEndTouching,
    expect(isDateMillisRangesOverlap(getRange(1, 4), getRange(2, 4)), true);
    expect(isDateMillisRangesOverlap(getRange(1, 4), getRange(4, null)), false);
    // EndInside,
    expect(isDateMillisRangesOverlap(getRange(2, 5), getRange(1, 4)), true);
    expect(isDateMillisRangesOverlap(getRange(2, 5), getRange(4, 1)), true);
    expect(isDateMillisRangesOverlap(getRange(5, 2), getRange(1, 4)), true);
    expect(isDateMillisRangesOverlap(getRange(5, 2), getRange(4, 1)), true);
    // EndTouching,
    expect(isDateMillisRangesOverlap(getRange(2, 3), getRange(1, 2)), false);
    // Before,
    expect(isDateMillisRangesOverlap(getRange(3, 4), getRange(1, 2)), false);
    expect(isDateMillisRangesOverlap(getRange(4, 3), getRange(1, 2)), false);
    expect(isDateMillisRangesOverlap(getRange(3, 4), getRange(2, 1)), false);
    expect(isDateMillisRangesOverlap(getRange(4, 3), getRange(2, 1)), false);
    expect(isDateMillisRangesOverlap(getRange(4, 3), getRange(2, null)), false);
  });

  test('durationFormatHHmmss', () {
    expect(Duration.zero.formatHHmmss(), '00:00:00');
    expect(Duration(days: 1, hours: 4, minutes: 3).formatHHmmss(), '28:03:00');
    expect(Duration(days: 10, hours: 4, minutes: 3).formatHHmmss(), '244:03:00');
    expect(Duration(days: 10, hours: 4, minutes: 3, seconds: 80).formatHHmmss(), '244:04:20');
  });
}
