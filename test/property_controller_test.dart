import 'dart:async';

import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:quadrate_tools/service/property_controller.dart';

void main() {
  test('Value output', () async {
    final propertyController = PropertyController<bool, String>(true);
    scheduleMicrotask(() {
      propertyController.value = false;
    });
    await expectLater(
        propertyController.valueOutput, emitsInOrder(<bool>[true, false]));
  });

  test('Value formatted output', () async {
    final propertyController = PropertyController<bool, String>(true);
    scheduleMicrotask(() {
      propertyController.value = false;
    });
    await expectLater(propertyController.formatted,
        emitsInOrder(<String>['true', 'false']));
  });

  test('Value filter', () async {
    final propertyController = PropertyController<int, String>(0,
        valueFilter: (value) => value == 2 ? value : 1);
    scheduleMicrotask(() {
      propertyController.value = 1;
      propertyController.value = 2;
      propertyController.value = 3;
    });
    await expectLater(
        propertyController.valueOutput, emitsInOrder(<int>[1, 1, 2, 1]));
  });

  test('Is valid stream', () async {
    final propertyController = PropertyController<int, String>(0,
        validator: (value) => value == 2 ? null : 'error');
    scheduleMicrotask(() {
      propertyController.value = 1;
      propertyController.value = 2;
      propertyController.value = 0;
    });
    await expectLater(
        propertyController.isValid, emitsInOrder(<bool>[false, false, true, false]));
  });

  test('Filter and validator combined', () async {
    final propertyController = PropertyController<int, String>(0,
        valueFilter: (value) => value == 0 ? 2 : value,
        validator: (value) => value == 1 ? null : 'error');
    scheduleMicrotask(() {
      propertyController.value = 1;
      propertyController.value = 2;
      propertyController.value = 0;
    });
    await expectLater(
        propertyController.isValid, emitsInOrder(<bool>[false, true, false]));
  });

  test('Is data changed stream', () async {
    final propertyController = PropertyController<int, String>(0);
    scheduleMicrotask(() {
      propertyController.value = 1;
      propertyController.value = 2;
      propertyController.value = 0;
    });
    await expectLater(
        propertyController.isDataChanged, emitsInOrder(<bool>[false, true]));
  });

  test('Value formatter', () async {
    final propertyController =
    PropertyController<int, String>(0, valueFormatter: (value) => TextSpan(text: '$value value'));
    scheduleMicrotask(() {
      propertyController.value = 1;
      propertyController.value = 2;
      propertyController.value = 0;
    });
    await expectLater(propertyController.formatted,
        emitsInOrder(<String>['0 value', '1 value', '2 value', '0 value']));
  });

  test('Validation message', () async {
    final propertyController = PropertyController<int, String>(0,
        validator: (value) => value == 1 ? null : '$value error');
    scheduleMicrotask(() {
      propertyController.value = 1;
      propertyController.value = 2;
      propertyController.value = 0;
    });
    await expectLater(propertyController.validationMsg,
        emitsInOrder(<String?>['0 error', null, '2 error', '0 error']));

    scheduleMicrotask(() {
      propertyController.showValidationMsgValue = false;
      propertyController.value = 1;
      propertyController.value = 2;
      propertyController.value = 0;
    });

    await expectLater(propertyController.validationMsg,
        emitsInOrder(<String?>['0 error', null]));
  });
}
