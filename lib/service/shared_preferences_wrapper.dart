import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesWrapper {
  final SharedPreferences _prefs;

  SharedPreferencesWrapper({required SharedPreferences sharedPreferences})
      : _prefs = sharedPreferences;

  T? get<T>(String key, {T? valueDefault}) {
    if (T == String) return (_prefs.getString(key) as T?) ?? valueDefault;
    if (T == bool) return (_prefs.getBool(key) as T?) ?? valueDefault;
    if (T == double) return (_prefs.getDouble(key) as T?) ?? valueDefault;
    if (T == int) return (_prefs.getInt(key) as T?) ?? valueDefault;
    if (<String>[] is T) {
      return (_prefs.getStringList(key) as T?) ?? valueDefault;
    }
    if (<int>[] is T) {
      return (_prefs.getStringList(key)?.fold<List<int>>(<int>[],
              (idsList, channelId) {
            final parsedId = int.tryParse(channelId);
            if (parsedId != null) {
              idsList.add(parsedId);
            }
            return idsList;
          }) as T?) ??
          valueDefault;
    }

    throw Exception('Undefined type to GET from settings');
  }

  Future<bool> set<T>(String key, T? value) {
    if (value == null) {
      remove(key);
      return Future.value(true);
    } else {
      if (T == String) return _prefs.setString(key, value as String);
      if (T == bool) return _prefs.setBool(key, value as bool);
      if (T == double) return _prefs.setDouble(key, value as double);
      if (T == int) return _prefs.setInt(key, value as int);
      if (<String>[] is T) {
        return _prefs.setStringList(key, value as List<String>);
      }
      if (<int>[] is T) {
        return _prefs.setStringList(
            key,
            (value as List<int>?)
                ?.map<String>((intValue) => intValue.toString())
                .toList() as List<String>);
      }
      throw Exception('Undefined type to SET in settings');
    }
  }

  Future<bool> remove(String key) => _prefs.remove(key);

  Future<bool> clear() => _prefs.clear();

  Set<String> getKeys() => _prefs.getKeys();

  bool containsKey(String key) => _prefs.containsKey(key);
}
