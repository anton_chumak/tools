import 'package:quadrate_tools/service/dispose_helper.dart';
import 'package:quadrate_tools/service/helpers.dart';
import 'package:quadrate_tools/service/property_controller.dart';
import 'package:rxdart/rxdart.dart';

/// Базовый класс для value объектов с данными для форм.
/// TODO create generic parameter for error message
class FormData with DisposeHelper {
  final String? _title;
  final String? formDescription;

  late final BehaviorSubject<bool> _isControllerDataValid; // ignore: close_sinks
  late final BehaviorSubject<bool> _isControllerDataChanged; // ignore: close_sinks
  late final BehaviorSubject<bool> _showValidationMsg;

  FormData(
      {String? title,
      required List<PropertyController> initControllers,
      this.formDescription,
      bool showValidationMsg = false})
      : _title = title {
    controllers.addAll(initControllers);
    addSubject(_showValidationMsg = BehaviorSubject.seeded(showValidationMsg));
    showValidationMsgValue = showValidationMsg;


    addSubject(_isControllerDataValid = BehaviorSubject<bool>.seeded(
        controllers
            .map<bool>((controller) => controller.isValid.value)
            .fold(true, (prev, element) => prev && element)));

    addSubscription(combineBooleanStreamsAnd(
        controllers.map<Stream<bool>>((controller) => controller.isValid),
        _isControllerDataValid));

    addSubject(_isControllerDataChanged = BehaviorSubject<bool>.seeded(
        controllers
            .map<bool>((controller) => controller.isDataChanged.value)
            .fold(false, (prev, element) => prev || element)));

    addSubscription(combineBooleanStreamsOr(
        controllers.map<Stream<bool>>((controller) => controller.isDataChanged),
        _isControllerDataChanged));
  }

  String? get title => _title;

  factory FormData.copy(FormData formData) {
    return FormData(
        title: formData.title,
        initControllers: formData.controllers
            .map<PropertyController>((origController) => origController.clone())
            .toList());
  }

  BehaviorSubject<bool> get isValid => _isControllerDataValid;

  BehaviorSubject<bool> get isDataChanged => _isControllerDataChanged;

  set dataChanged(bool value) {
    for (PropertyController controller in controllers) {
      controller.isDataChanged.add(value);
    }
  }

  @override
  // ignore: must_call_super
  void addController(PropertyController controller) {
    throw UnsupportedError('Add controllers in constructor');
  }

  Stream<bool> get showValidationMsg => _showValidationMsg;

  bool get showValidationMsgValue => _showValidationMsg.value;

  set showValidationMsgValue(bool newValue) {
    _showValidationMsg.add(newValue);
    controllers.forEach((controller) {
      controller.showValidationMsgValue = newValue;
    });
  }
}
