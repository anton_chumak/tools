import 'dart:async';

import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:quiver/strings.dart';
import 'package:rxdart/rxdart.dart';

/// Однострочный текст
///
/// В случае переполнения строки применяется эффект пропадания
class OneLineText extends Text {
  const OneLineText(String data,
      {TextStyle? style,
        TextAlign? textAlign,
        TextOverflow overflow = TextOverflow.fade})
      : super(
    data,
    textAlign: textAlign,
    style: style,
    overflow: overflow,
    maxLines: 1,
    softWrap: false,
  );
}

class DummyStatefulWidget extends StatefulWidget {
  final VoidCallback? onInit;
  final Widget child;

  const DummyStatefulWidget({Key? key, required this.child, this.onInit}) : super(key: key);

  @override
  DummyStatefulWidgetState createState() => DummyStatefulWidgetState();
}

class DummyStatefulWidgetState extends State<DummyStatefulWidget> {

  @override
  void initState() {
    super.initState();
    widget.onInit?.call();
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }
}

typedef ContentBuilder<T> = Widget Function(BuildContext context, T value);

class StreamBuilderHelper<T> extends StatelessWidget {
  final Stream<T> stream;
  final T? initialData;
  final Widget whenEmpty;
  final ContentBuilder<T> builder;

  const StreamBuilderHelper(
      {super.key,
        required this.stream,
        this.whenEmpty = const SizedBox.expand(),
        required this.builder, this.initialData});

  @override
  Widget build(BuildContext context) {
    final _stream = stream;
    return StreamBuilder<T>(
        stream: stream,
        initialData: initialData ?? (_stream is ValueStream<T> ? _stream.valueOrNull : null),
        builder: (context, streamSnapshot) {
          final streamSnapshotData = streamSnapshot.data;
          return streamSnapshotData == null
              ? whenEmpty
              : builder(context, streamSnapshotData);
        });
  }
}


String? stringValueFilter (String? value) {
  final trimmedValue = value?.trim();
  return isEmpty(value) ? null : trimmedValue;
}

/// Добавляет разделитель между виджетами
///
/// Код скопирован из ListTile.divideTiles и изменен
Iterable<Widget> divideTiles(
    {required Iterable<Widget> tiles,
    required Widget divider,
    bool leading = false,
    bool trailing = false}) sync* {
  final iterator = tiles.iterator;
  final isNotEmpty = iterator.moveNext();
  if (isNotEmpty) {
    if (leading) {
      yield divider;
    }
    var tile = iterator.current;
    while (iterator.moveNext()) {
      yield tile;
      yield divider;
      tile = iterator.current;
    }
    yield tile;
    if (trailing) {
      yield divider;
    }
  }
}

/// Получаем форму слова исходя из числительного.
///
/// Пример.
/// 1 - исполните(ль). 2 - исполните(ля)
/// В функцию передается список слов для трех вариантов и число.
/// Функция возвращает нужный вариант слова.
/// Пример: [1 - день, 2 - дня, много - дней]
String selectWordFormByNum(List<String> titles, int numeral) => titles[selectWordFormIndexByNum(numeral)];

int selectWordFormIndexByNum(int numeral) {
  final cases = [2, 0, 1, 1, 1, 2];
  return ((numeral % 100 > 4 && numeral % 100 < 20) ? 2 : cases[(numeral % 10 < 5) ? numeral % 10 : 5]);
}

int nullAwareIgnoreCaseCompare(String? a, String? b) {
  if (a == null && b == null) return 0;
  return a == null ? 1 : (b == null ? -1 : a.toLowerCase().compareTo(b.toLowerCase()));
}

extension CloseTo on double {
  bool closeTo(double other, {double delta = 0.00001}) {
    return (this-other).abs() < delta;
  }
}

extension ContainsIgnoreCase on String {
  bool containsIgnoreCase(String other) {
    return toLowerCase().contains(other.toLowerCase());
  }
}

extension ResetToDate on DateTime {
  DateTime resetToDate() {
    return isUtc ? DateTime.utc(year, month, day) : DateTime(year, month, day);
  }

  DateTime resetToDateEOD() {
    return isUtc
        ? DateTime.utc(year, month, day, 23, 59, 59, 999, 999)
        : DateTime(year, month, day, 23, 59, 59, 999, 999);
  }
}

/// Возвращает true, если у двух дат одно и то же число
bool isTheSameDay(DateTime dt1, DateTime dt2) {
  return dt1.toLocal().day == dt2.toLocal().day &&
      dt1.toLocal().month == dt2.toLocal().month &&
      dt1.toLocal().year == dt2.toLocal().year;
}

/// Returns `date` in UTC format, without its time part.
DateTime normalizeDate(DateTime date) {
  return DateTime.utc(date.year, date.month, date.day);
}

class DateRange {
  late final DateTime _start;
  late final DateTime? _end;

  DateRange({required DateTime start, DateTime? end}) {
    if (end == null) {
      _start = start;
      _end = null;
    } else {
      final isStartBeforeEnd = start.isBefore(end);
      _start = isStartBeforeEnd ? start : end;
      _end = isStartBeforeEnd ? end : start;
    }
  }

  bool get isSingleDay => _end == null;

  DateTime get start => _start;

  DateTime? get end => _end;
}

class DateMillisRange {
  late final int _start;
  late final int? _end;

  DateMillisRange({required int start, int? end}) {
    if (end == null) {
      _start = start;
      _end = null;
    } else {
      final isStartBeforeEnd = start < end;
      _start = isStartBeforeEnd ? start : end;
      _end = isStartBeforeEnd ? end : start;
    }
  }

  bool get isSingleDay => _end == null;

  int get start => _start;

  int? get end => _end;
}

bool isDateRangesOverlap(DateRange rangeA, DateRange rangeB) {
  bool result;
  if (rangeA.isSingleDay && rangeB.isSingleDay && rangeA.start.isAtSameMomentAs(rangeB.start)) {
    result = true;
  } else {
    // https://stackoverflow.com/questions/325933/determine-whether-two-date-ranges-overlap
    // (StartA < EndB)  and  (EndA > StartB)
    result = rangeA.start.isBefore(rangeB.end ?? rangeB.start) && (rangeA.end ?? rangeA.start ).isAfter(rangeB.start);
  }
  return result;
}

bool isDateMillisRangesOverlap(DateMillisRange rangeA, DateMillisRange rangeB) {
  bool result;
  if (rangeA.isSingleDay && rangeB.isSingleDay && rangeA.start == rangeB.start) {
    result = true;
  } else {
    // https://stackoverflow.com/questions/325933/determine-whether-two-date-ranges-overlap
    // (StartA < EndB)  and  (EndA > StartB)
    result = rangeA.start < (rangeB.end ?? rangeB.start) && (rangeA.end ?? rangeA.start ) > rangeB.start;
  }
  return result;
}

/// Делает заглавной первую букву строки, остальные буквы строчными
String? capFirstLowRest(String? input) => input == null || input.isEmpty
    ? input
    : '${input[0].toUpperCase()}${input.substring(1).toLowerCase()}';

/// Делает заглавной первую букву строки, остальные буквы без изменений
String? capFirstRestUnchanged(String? input) =>
    input == null || input.isEmpty ? input : '${input[0].toUpperCase()}${input.substring(1)}';

StreamSubscription<bool> combineBooleanStreamsAnd(
    Iterable<Stream<bool>> streams, BehaviorSubject<bool> resultStream) {
  return _combineBooleanStreams(
      streams, resultStream, (prev, element) => prev && element);
}

StreamSubscription<bool> combineBooleanStreamsOr(
    Iterable<Stream<bool>> streams, BehaviorSubject<bool> resultStream) {
  return _combineBooleanStreams(
      streams, resultStream, (prev, element) => prev || element);
}

StreamSubscription<bool> _combineBooleanStreams(
    Iterable<Stream<bool>> streams,
    BehaviorSubject<bool> resultStream,
    bool Function(bool value, bool element) combine) {
  return CombineLatestStream<bool, bool>(streams, (values) {
    return values.reduce(combine);
  }).listen((data) {
    resultStream.add(data);
  });
}

// https://blog.usejournal.com/creating-a-custom-color-swatch-in-flutter-554bcdcb27f3
MaterialColor createMaterialColor(Color color) {
  List strengths = <double>[.05];
  final swatch = <int, Color>{};
  final int r = color.red, g = color.green, b = color.blue;

  for (int i = 1; i < 10; i++) {
    strengths.add(0.1 * i);
  }
  strengths.forEach((strength) {
    final double ds = 0.5 - strength;
    swatch[(strength * 1000).round()] = Color.fromRGBO(
      r + ((ds < 0 ? r : (255 - r)) * ds).round(),
      g + ((ds < 0 ? g : (255 - g)) * ds).round(),
      b + ((ds < 0 ? b : (255 - b)) * ds).round(),
      1,
    );
  });
  return MaterialColor(color.value, swatch);
}

Future<void> throwDelayedException({bool doThrow = true, Duration delay = const Duration(seconds: 2)}) async {
  await Future.delayed(delay);
  if (doThrow) {
    throw Exception('blah');
  }
}

// measureTimeSync(() {    return
// });
T measureTimeSync<T>(T Function() functionBody) {
  print('-- Begin');
  final beginMoment = DateTime.now();
  final result = functionBody.call();
  print(
      '-- End. Time: ${DateTime.now().millisecondsSinceEpoch - beginMoment.millisecondsSinceEpoch} ms');
  return result;
}

// await measureTimeAsync(() async  {    return
// });
Future<T> measureTimeAsync<T>(Future<T> Function() functionBody) async {
  print('-- Begin');
  final beginMoment = DateTime.now();
  final result = await functionBody.call();
  print(
      '-- End. Time: ${DateTime.now().millisecondsSinceEpoch - beginMoment.millisecondsSinceEpoch} ms');
  return result;
}

/// Контейнер для ответа сервера со списком объектов
class EntityListResult<T> {
  /// Общее количество объектов, удовлетворяющих условиям фильтрации
  final int total;

  /// Подмножество элементов списка
  final List<T> items;

  EntityListResult(this.total, this.items);
}

extension HexColor on Color {
  /// String is in the format "aabbcc" or "ffaabbcc" with an optional leading "#".
  static Color fromHex(String hexString) {
    final buffer = StringBuffer();
    if (hexString.length == 6 || hexString.length == 7) buffer.write('ff');
    buffer.write(hexString.replaceFirst('#', ''));
    return Color(int.parse(buffer.toString(), radix: 16));
  }

  /// Prefixes a hash sign if [leadingHashSign] is set to `true` (default is `true`).
  String toHex({bool leadingHashSign = true}) => '${leadingHashSign ? '#' : ''}'
      '${alpha.toRadixString(16).padLeft(2, '0')}'
      '${red.toRadixString(16).padLeft(2, '0')}'
      '${green.toRadixString(16).padLeft(2, '0')}'
      '${blue.toRadixString(16).padLeft(2, '0')}';
}

extension SizedBoxX on num {
  SizedBox get sbHeight => SizedBox(height: toDouble());

  SizedBox get sbWidth => SizedBox(width: toDouble());
}

extension DurationFormat on Duration {
  String _twoDigits(int n) => n.toString().padLeft(2, '0');

  String formatHHmmss() {
    return '${_twoDigits(inHours)}:${_twoDigits(inMinutes.remainder(60))}:${_twoDigits(inSeconds.remainder(60))}';
  }
}

extension ShareValueExtension<T> on RxObjectMixin<T> {
  ValueStream<T> get shareValueSeeded => stream.shareValueSeeded(value);
}