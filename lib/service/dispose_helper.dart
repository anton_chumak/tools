import 'dart:async';

import 'package:flutter/material.dart';
import 'package:quadrate_tools/service/property_controller.dart';
import 'package:rxdart/rxdart.dart';

mixin DisposeHelper {
  final _subscriptions = CompositeSubscription();
  final List<PropertyController> controllers = [];
  final List<Subject> _subjects = [];

  @mustCallSuper
  void dispose() {
    _subscriptions.dispose();
    _subjects.forEach((subject) => subject.close());
    controllers.forEach((controller) => controller.dispose());
  }

  @mustCallSuper
  void addSubscription(StreamSubscription subscription) {
    _subscriptions.add(subscription);
  }

  @mustCallSuper
  void addController(PropertyController controller) {
    controllers.add(controller);
  }

  @mustCallSuper
  void addSubject(Subject subject) {
    _subjects.add(subject);
  }
}