import 'dart:async';

import 'package:flutter/src/painting/text_span.dart';
import 'package:quadrate_tools/service/property_controller.dart';
import 'package:rxdart/src/subjects/behavior_subject.dart';
import 'package:rxdart/src/subjects/subject.dart';

class PropertyControllerAdapter<A, T, V> implements PropertyController<A, V> {
  final PropertyController<T, V> controller;
  final T Function(A adaptedValue, T currentOriginalValue) fromAdaptedTypeFormatter;
  final A Function(T originValue) fromOriginTypeFormatter;

  PropertyControllerAdapter(
      {required this.controller,
      required this.fromAdaptedTypeFormatter,
      required this.fromOriginTypeFormatter});

  @override
  A get value => fromOriginTypeFormatter(controller.value);

  @override
  set value(A value) {
    controller.value = fromAdaptedTypeFormatter(value, controller.value);
  }

  @override
  TextSpan Function(A value) get valueFormatter => (A value) {
        return controller.valueFormatter(fromAdaptedTypeFormatter(value, controller.value));
      };

  @override
  Stream<A> get valueOutput => controller.valueOutput.map(fromOriginTypeFormatter);

  @override
  PropertyController<A, V> clone() => PropertyControllerAdapter(
      controller: controller.clone(),
      fromAdaptedTypeFormatter: fromAdaptedTypeFormatter,
      fromOriginTypeFormatter: fromOriginTypeFormatter);

  // the rest of the methods are just calling wrapped controller

  @override
  String? get comment => controller.comment;

  @override
  List<PropertyController> get controllers => controller.controllers;

  @override
  void dispose() => controller.dispose();

  @override
  Stream<TextSpan> get formatted => controller.formatted;

  @override
  TextSpan get formattedValue => controller.formattedValue;

  @override
  BehaviorSubject<bool> get isDataChanged => controller.isDataChanged;

  @override
  bool get isMandatory => controller.isMandatory;

  @override
  BehaviorSubject<bool> get isValid => controller.isValid;

  @override
  bool get isValidValue => controller.isValidValue;

  @override
  Stream<bool> get showValidationMsg => controller.showValidationMsg;

  @override
  bool get userEnter => controller.userEnter;

  @override
  void validate() => controller.validate();

  @override
  Stream<V?> get validationMsg => controller.validationMsg;

  @override
  Object? get valueType => controller.valueType;

  @override
  bool get showValidationMsgValue => controller.showValidationMsgValue;

  @override
  void addController(PropertyController controller) => controller.addController(controller);

  @override
  void addSubject(Subject subject) => controller.addSubject(subject);

  @override
  void addSubscription(StreamSubscription subscription) => controller.addSubscription(subscription);

  @override
  String get caption => controller.caption;

  @override
  set showValidationMsgValue(bool value) => controller.showValidationMsgValue = value;
}
