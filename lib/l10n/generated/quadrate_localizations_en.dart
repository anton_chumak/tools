import 'quadrate_localizations.dart';

/// The translations for English (`en`).
class QuadrateLocalizationEn extends QuadrateLocalization {
  QuadrateLocalizationEn([String locale = 'en']) : super(locale);

  @override
  String get error => 'Error';

  @override
  String get connectionFailure => 'Connection failure';

  @override
  String get checkNetworkAndRetry => 'Check network connection and retry';

  @override
  String get serverFailure => 'Server failure';

  @override
  String get tryAgain => 'Please try again in few moments';
}
