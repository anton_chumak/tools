import 'quadrate_localizations.dart';

/// The translations for Russian (`ru`).
class QuadrateLocalizationRu extends QuadrateLocalization {
  QuadrateLocalizationRu([String locale = 'ru']) : super(locale);

  @override
  String get error => 'Ошибка';

  @override
  String get connectionFailure => 'Ошибка соединения';

  @override
  String get checkNetworkAndRetry => 'Проверьте сеть и повторите попытку';

  @override
  String get serverFailure => 'Ошибка сервера';

  @override
  String get tryAgain => 'Попробуйте еще раз через некоторое время';
}
