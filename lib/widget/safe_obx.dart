import 'package:flutter/widgets.dart';
import 'package:get/get.dart' as g;

/// Wrapper around dummy obs value to avoid situation when no obs.value called in builder body.
/// Regular Obx throws in this situation.
class SafeObx extends g.Obx {
  static final dummyObs = 0.obs;

  const SafeObx(super.builder, {super.key});

  @override
  Widget build() {
    dummyObs.value;
    return builder();
  }
}

// This is just immediately invalidate all existing Obs widgets in a class where SafeObx appears
class Obx extends g.Obx {
  const Obx(super.builder, {super.key});
}