import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:quadrate_tools/service/property_controller.dart';
import 'package:quadrate_tools/widget/immutable_text_form_field.dart';
import 'package:quiver/strings.dart';

class CurrencyPropertyFormField<V> extends StatelessWidget {
  final PropertyController<int?, V> propertyController;
  final InputDecoration _decoration;
  final bool autofocus;
  final TextStyle? style;
  final bool showErrorMessage;
  final TextAlign textAlign;
  final ErrorMessageMapper<V> _errorMessageMapper;

  const CurrencyPropertyFormField(
      {Key? key,
      required this.propertyController,
      InputDecoration? decoration,
      this.autofocus = false,
      this.style,
      this.showErrorMessage = false,
      this.textAlign = TextAlign.start,
        ErrorMessageMapper<V>? errorMessageMapper})
      : _decoration = decoration ?? const InputDecoration(),
        _errorMessageMapper = errorMessageMapper ?? defaultErrorMessageMapper<V>,
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<String?>(
        stream: propertyController.validationMsg
            .distinct()
            .map((errorMsg) => _errorMessageMapper(context, errorMsg)),
        builder: (context, errorMsgSnapshot) {
          final errorMsgSnapshotData = errorMsgSnapshot.data;
          return CurrencyFormField(
              textAlign: textAlign,
              autofocus: autofocus,
              style: !showErrorMessage && errorMsgSnapshotData != null
                  ? style?.copyWith(color: Theme.of(context).colorScheme.error)
                  : style,
              initValue: propertyController.value,
              decoration:
                  showErrorMessage ? _decoration.copyWith(errorText: errorMsgSnapshotData) : _decoration,
              onChanged: (newValue) {
                propertyController.value = newValue;
              });
        });
  }
}

class CurrencyFormField extends StatefulWidget {
  final bool autofocus;
  final ValueChanged<int?>? onChanged;
  final int? initValue;
  final String? errorText;
  final InputDecoration? decoration;
  final TextStyle? style;
  final TextInputAction textInputAction;
  final ValueChanged<String>? onSubmitted;
  final TextAlignVertical? textAlignVertical;
  final TextAlign textAlign;

  const CurrencyFormField(
      {Key? key,
      this.initValue,
      this.errorText,
      this.onChanged,
      this.decoration,
      this.autofocus = false,
      this.style,
      this.textInputAction = TextInputAction.next,
      this.onSubmitted,
      this.textAlignVertical,
      this.textAlign = TextAlign.left})
      : super(key: key);

  @override
  _CurrencyFormFieldState createState() => _CurrencyFormFieldState();

  static String? intToStringTransformer(int? intValue) {
    String? result;
    if (intValue != null) {
      result = intValue % 100 == 0 ? (intValue ~/ 100).toString() : (intValue / 100).toStringAsFixed(2);
    }
    return result;
  }

  static int? stringToIntTransformer(String text) {
    final decimalPointPosition = CurrencyTextInputFormatter.findDecimalPointPosition(text);

    int? parseWholePart(String _value) {
      int? _result;
      final parsedValue = int.tryParse(_value);
      if (parsedValue != null) {
        _result = parsedValue * 100;
      }
      return _result;
    }

    int? result;
    if (decimalPointPosition < 0) {
      result = parseWholePart(text);
    } else {
      final int? wholePart = parseWholePart(text.substring(0, decimalPointPosition));
      final int? decimalPart =
          int.tryParse(text.substring(decimalPointPosition + 1).padRight(2, '0').substring(0, 2));
      result = wholePart == null || decimalPart == null ? null : wholePart + decimalPart;
    }
    return result;
  }
}

class _CurrencyFormFieldState extends State<CurrencyFormField> {
  late TextEditingController textController;

  @override
  void initState() {
    super.initState();
    final initStringValue = CurrencyFormField.intToStringTransformer(widget.initValue) ?? '';
    final textEditingValue = TextEditingValue(
        text: initStringValue, selection: TextSelection.collapsed(offset: initStringValue.length));
    textController = TextEditingController.fromValue(textEditingValue);
    textController.addListener(() {
      if (widget.onChanged != null) {
        widget.onChanged!(CurrencyFormField.stringToIntTransformer(textController.text));
      }
    });
  }

  @override
  void dispose() {
    textController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return TextFormField(
        textAlign: widget.textAlign,
        autofocus: widget.autofocus,
        textAlignVertical: widget.textAlignVertical,
        style: widget.style,
        controller: textController,
        decoration: widget.decoration ?? InputDecoration(errorText: widget.errorText),
        keyboardType: const TextInputType.numberWithOptions(decimal: true),
        autocorrect: false,
        inputFormatters: <TextInputFormatter>[
          FilteringTextInputFormatter.allow(RegExp(r'[0-9,.]')),
          CurrencyTextInputFormatter()
        ],
        textInputAction: widget.textInputAction,
        onFieldSubmitted: widget.onSubmitted);
  }
}

class CurrencyTextInputFormatter extends TextInputFormatter {
  late Rule formatRulesStack;

  CurrencyTextInputFormatter() {
    formatRulesStack = RemoveSecondDecimalPointRule(null);
    formatRulesStack = RemoveDecimalPartRule(formatRulesStack);
    formatRulesStack = LeadingZerosRule(formatRulesStack);
    formatRulesStack = DecimalPartRule(formatRulesStack);
  }

  @override
  TextEditingValue formatEditUpdate(TextEditingValue oldValue, TextEditingValue newValue) {
    final valueWrapper = TextEditingValueWrapper(oldValue: oldValue, newValue: newValue);
    formatRulesStack.applyRule(valueWrapper);
    return valueWrapper.newValue;
  }

  static int findDecimalPointPosition(String source, [int start = 0]) =>
      source.indexOf(RegExp(r'[.,]'), start);
}

class TextEditingValueWrapper {
  TextEditingValue oldValue;
  TextEditingValue newValue;
  late int newValueDecimalPointPosition;

  TextEditingValueWrapper({required this.oldValue, required this.newValue}) {
    newValueDecimalPointPosition = CurrencyTextInputFormatter.findDecimalPointPosition(newValue.text);
  }
}

abstract class Rule {
  void applyRule(TextEditingValueWrapper textEditingValueWrapper);
}

abstract class BaseFormatRule implements Rule {
  final Rule? prevRule;

  BaseFormatRule(this.prevRule);

  @override
  @mustCallSuper
  // Call super first
  void applyRule(TextEditingValueWrapper textEditingValueWrapper) {
    prevRule?.applyRule(textEditingValueWrapper);
  }
}

class RemoveSecondDecimalPointRule extends BaseFormatRule {
  RemoveSecondDecimalPointRule(Rule? nextRule) : super(nextRule);

  @override
  void applyRule(TextEditingValueWrapper textEditingValueWrapper) {
    super.applyRule(textEditingValueWrapper);

    final newValueDecimalPointPosition = textEditingValueWrapper.newValueDecimalPointPosition;
    final text = textEditingValueWrapper.newValue.text;
    final secondPointPosition =
        CurrencyTextInputFormatter.findDecimalPointPosition(text, newValueDecimalPointPosition + 1);
    if (secondPointPosition > -1) {
      textEditingValueWrapper.newValue = textEditingValueWrapper.newValue
          .replaced(TextRange(start: secondPointPosition, end: text.length), '');
    }
  }
}

class RemoveDecimalPartRule extends BaseFormatRule {
  RemoveDecimalPartRule(Rule? nextRule) : super(nextRule);

  @override
  void applyRule(TextEditingValueWrapper textEditingValueWrapper) {
    super.applyRule(textEditingValueWrapper);
    final newValueTextLength = textEditingValueWrapper.newValue.text.length;
    if (CurrencyTextInputFormatter.findDecimalPointPosition(textEditingValueWrapper.oldValue.text) >
            -1 &&
        textEditingValueWrapper.newValueDecimalPointPosition < 0 &&
        textEditingValueWrapper.newValue.selection.isCollapsed &&
        textEditingValueWrapper.newValue.selection.start == newValueTextLength - 2) {
      textEditingValueWrapper.newValue = textEditingValueWrapper.newValue
          .replaced(TextRange(start: newValueTextLength - 2, end: newValueTextLength), '');
    }
  }
}

class LeadingZerosRule extends BaseFormatRule {
  LeadingZerosRule(Rule? nextRule) : super(nextRule);

  @override
  void applyRule(TextEditingValueWrapper textEditingValueWrapper) {
    super.applyRule(textEditingValueWrapper);
    final _decimalPointPosition = textEditingValueWrapper.newValueDecimalPointPosition;
    final text = textEditingValueWrapper.newValue.text;
    if (!isBlank(text)) {
      if (_decimalPointPosition == 0) {
        textEditingValueWrapper.newValue =
            textEditingValueWrapper.newValue.replaced(const TextRange.collapsed(0), '0');
        textEditingValueWrapper.newValueDecimalPointPosition = 1;
      } else {
        final intPartStr = _decimalPointPosition < 0 ? text : text.substring(0, _decimalPointPosition);
        if (text.startsWith('0')) {
          final leadingZerosRegExp = RegExp(r'^0+');
          final leadingZerosMatch = leadingZerosRegExp.firstMatch(intPartStr);
          if (leadingZerosMatch != null) {
            final String leadingZerosStr = leadingZerosMatch[0]!;
            final lengthToRemove = leadingZerosStr.length - (intPartStr == leadingZerosStr ? 1 : 0);
            textEditingValueWrapper.newValue =
                textEditingValueWrapper.newValue.replaced(TextRange(start: 0, end: lengthToRemove), '');
            textEditingValueWrapper.newValueDecimalPointPosition = _decimalPointPosition < 0
                ? _decimalPointPosition
                : _decimalPointPosition - lengthToRemove;
          }
        }
      }
    }
  }
}

class DecimalPartRule extends BaseFormatRule {
  DecimalPartRule(Rule? nextRule) : super(nextRule);

  @override
  void applyRule(TextEditingValueWrapper textEditingValueWrapper) {
    super.applyRule(textEditingValueWrapper);
    final _decimalPointPosition = textEditingValueWrapper.newValueDecimalPointPosition;
    if (_decimalPointPosition >= 0) {
      final text = textEditingValueWrapper.newValue.text;
      final decimalPartStr = text.substring(_decimalPointPosition + 1);
      if (decimalPartStr.length < 2) {
        final selection = textEditingValueWrapper.newValue.selection;
        textEditingValueWrapper.newValue = textEditingValueWrapper.newValue
            .replaced(TextRange.collapsed(text.length), ''.padRight(2 - decimalPartStr.length, '0'))
            .copyWith(selection: selection);
      } else if (decimalPartStr.length > 2) {
        textEditingValueWrapper.newValue = textEditingValueWrapper.newValue
            .replaced(TextRange(start: _decimalPointPosition + 3, end: text.length), '');
      }
    }
  }
}
