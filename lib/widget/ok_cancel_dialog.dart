import 'package:flutter/material.dart';

enum ConfirmationDialogResult { cancel, ok }

class OkCancelDialog extends StatelessWidget {
  final String title;
  final String? content;
  final Widget? contentWidget;
  final String? cancelButtonTitle;
  final String okButtonTitle;

  const OkCancelDialog(
      {super.key,
      required this.title,
      this.cancelButtonTitle,
      required this.okButtonTitle,
      this.content,
      this.contentWidget});

  @override
  Widget build(BuildContext context) {
    final contentTextLocal = this.content;
    final resultContent = contentWidget ?? (contentTextLocal == null ? null : Text(contentTextLocal));
    final cancelButtonTitleLocal = cancelButtonTitle;
    return AlertDialog(
        title: Text(title),
        content: resultContent == null ? null : SingleChildScrollView(child: resultContent),
        actions: <Widget>[
          if (cancelButtonTitleLocal != null)
            TextButton(
                onPressed: () {
                  Navigator.pop(context, ConfirmationDialogResult.cancel);
                },
                child: Text(cancelButtonTitleLocal)),
          TextButton(
              onPressed: () {
                Navigator.pop(context, ConfirmationDialogResult.ok);
              },
              child: Text(okButtonTitle))
        ]);
  }
}
