import 'dart:ui';

import 'package:flutter/material.dart';

class CoachMarkRoute<T> extends PageRoute<T> {
  final Path path;
  final bool drawPath;
  final double? opacity;
  final double? blurSize;
  final Widget Function({required BuildContext context, required Path path})
      overlayContentBuilder;

  CoachMarkRoute(
      {required this.path,
      this.drawPath = true,
      required this.overlayContentBuilder,
      this.opacity,
      this.blurSize});

  @override
  bool get maintainState => true;

  @override
  Duration get transitionDuration => const Duration(milliseconds: 450);

  @override
  Color? get barrierColor => null;

  @override
  String? get barrierLabel => null;

  @override
  bool get opaque => false;

  @override
  Widget buildPage(BuildContext context, Animation<double> animation,
      Animation<double> secondaryAnimation) {
    final blurSizeLocal = blurSize;
    final opacityLocal = opacity;
    final invertedPath = drawPath
        ? Path.combine(PathOperation.difference,
            Path()..addRect(Offset.zero & MediaQuery.of(context).size), path)
        : Path()
      ..addRect(Offset.zero & MediaQuery.of(context).size);

    return Material(
        type: MaterialType.transparency,
        child: GestureDetector(
            behavior: HitTestBehavior.opaque,
            onTapDown: (d) => Navigator.of(context).pop(),
            child: IgnorePointer(
                child: AnimatedBuilder(
                    animation: animation,
                    builder: (BuildContext context, Widget? child) {
                      return Stack(
                          children: <Widget>[
                        if (blurSizeLocal != null)
                          ClipPath(
                              clipper: PathClipper(path: invertedPath),
                              child: BackdropFilter(
                                  filter: ImageFilter.blur(
                                      sigmaX: animation.value * blurSizeLocal,
                                      sigmaY: animation.value * blurSizeLocal),
                                  child: SizedBox.expand())),
                        if (opacityLocal != null)
                          CustomPaint(
                              painter: CoachMarkPainter(
                                  path: invertedPath,
                                  color: Colors.black.withOpacity(
                                      1 - opacityLocal * animation.value)),
                              child: SizedBox.expand()),
                        overlayContentBuilder(context: context, path: path)
                      ]);
                    }))));
  }
}

class CoachMarkPainter extends CustomPainter {
  final Path path;
  final Color color;

  CoachMarkPainter({required this.path, required this.color});

  @override
  void paint(Canvas canvas, Size size) {
    canvas.clipPath(path);
    canvas.drawColor(color, BlendMode.srcOver);
  }

  @override
  bool shouldRepaint(CoachMarkPainter old) => old.color != color;

  @override
  bool shouldRebuildSemantics(CoachMarkPainter oldDelegate) => false;
}

class PathClipper extends CustomClipper<Path> {
  final Path path;

  PathClipper({required this.path});

  @override
  Path getClip(Size size) => path;

  @override
  bool shouldReclip(covariant CustomClipper<Path> oldClipper) => false;
}
