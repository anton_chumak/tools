import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:quiver/strings.dart';

class AdvancedNetworkImage extends StatefulWidget {
  final String? src;
  final double progressStrokeWidth;
  final double progressSize;
  final Color? backgroundColor;
  final Color? progressColor;
  final Widget? imagePlaceholder;
  final BoxFit? fit;
  final bool useCache;

  const AdvancedNetworkImage(
      {super.key,
      this.src,
      this.progressStrokeWidth = 4.0,
      this.progressSize = 36,
      this.backgroundColor,
      this.progressColor,
      this.imagePlaceholder,
      this.fit,
      this.useCache = false});

  @override
  State<AdvancedNetworkImage> createState() => _AdvancedNetworkImageState();
}

class _AdvancedNetworkImageState extends State<AdvancedNetworkImage> {
  final imageKey = GlobalKey<State<Image>>();
  int cachedKeyValue = 0;

  @override
  Widget build(BuildContext context) {
    final srcLocal = widget.src;

    Widget buildProgress(double? progress) {
      return Container(
          alignment: Alignment.center,
          child: SizedBox(
              width: widget.progressSize,
              height: widget.progressSize,
              child: CircularProgressIndicator(
                  color: widget.progressColor,
                  strokeWidth: widget.progressStrokeWidth,
                  value: progress)));
    }

    return srcLocal == null || isBlank(srcLocal)
        ? widget.imagePlaceholder ?? SizedBox.shrink()
        : Material(
            color: widget.backgroundColor,
            child: widget.useCache
                ? CachedNetworkImage(
                    key: ValueKey<int>(cachedKeyValue),
                    fit: widget.fit ?? BoxFit.cover,
                    imageUrl: srcLocal,
                    progressIndicatorBuilder:
                        (BuildContext context, String url, DownloadProgress progress) =>
                            buildProgress(progress.progress),
                    errorWidget: (BuildContext context, String url, Object error) {
                      return InkWell(
                          onTap: () {
                            setState(() {
                              cachedKeyValue++;
                            });
                          },
                          child: Center(child: Icon(Icons.refresh, color: widget.progressColor)));
                    },
                    fadeOutDuration: const Duration(milliseconds: 100),
                    fadeInDuration: const Duration(milliseconds: 100))
                : Image.network(srcLocal, key: imageKey, fit: widget.fit ?? BoxFit.cover, loadingBuilder:
                    (BuildContext context, Widget child, ImageChunkEvent? loadingProgress) {
                    return loadingProgress == null
                        ? child
                        : buildProgress(loadingProgress.expectedTotalBytes != null
                            ? loadingProgress.cumulativeBytesLoaded / loadingProgress.expectedTotalBytes!
                            : null);
                  }, errorBuilder: (context, error, stackTrace) {
                    return InkWell(
                        onTap: () {
                          imageKey.currentState?.reassemble();
                        },
                        child: Center(child: Icon(Icons.refresh, color: widget.progressColor)));
                  }));
  }
}
