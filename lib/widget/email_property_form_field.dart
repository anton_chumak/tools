import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:quadrate_tools/quadrate_tools.dart';

class EmailPropertyFormField extends StatelessWidget {
  final PropertyController<String?, String> propertyController;
  final InputDecoration _decoration;

  const EmailPropertyFormField(
      {required this.propertyController, InputDecoration? decoration})
      : _decoration = decoration ?? const InputDecoration();

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<String?>(
        stream: propertyController.validationMsg.distinct(),
        builder: (context, errorMsgSnapshot) {
          return EmailFormField(
              initialValue: propertyController.value,
              decoration:
                  _decoration.copyWith(errorText: errorMsgSnapshot.data),
              onChanged: (newValue) {
                propertyController.value = newValue;
              });
        });
  }
}

class EmailFormField extends StatefulWidget {
  final TextStyle? style;
  final String? initialValue;
  final InputDecoration? decoration;
  final String? errorText;
  final bool autofocus;
  final ValueChanged<String>? onChanged;
  final TextInputAction textInputAction;

  const EmailFormField({
    Key? key,
    this.style,
    this.initialValue,
    this.decoration,
    this.errorText,
    this.autofocus = false,
    this.onChanged,
    this.textInputAction = TextInputAction.next,
  }) : super(key: key);

  @override
  _EmailFormFieldState createState() => _EmailFormFieldState();
}

class _EmailFormFieldState extends State<EmailFormField> {
  late TextEditingController _textEditingController;

  @override
  void initState() {
    super.initState();
    _textEditingController = TextEditingController(text: widget.initialValue);
    _textEditingController.addListener(() {
      final _onChanged = widget.onChanged;
      if (_onChanged != null) {
        _onChanged(_textEditingController.text);
      }
    });
  }

  @override
  void dispose() {
    _textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return TextField(
        style: widget.style,
        controller: _textEditingController,
        decoration: widget.decoration,
        keyboardType: TextInputType.emailAddress,
        autocorrect: false,
        inputFormatters: [
          FilteringTextInputFormatter.allow(
              RegExp("[a-zA-Z0-9@\\-_.!#\$%&'*+-/=?^`{|}~]")),
          FilteringTextInputFormatter.deny(RegExp('[,]'))
        ],
        textInputAction: widget.textInputAction);
  }
}
