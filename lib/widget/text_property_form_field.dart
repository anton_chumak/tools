import 'package:flutter/material.dart';
import 'package:quadrate_tools/quadrate_tools.dart';
import 'package:quadrate_tools/widget/immutable_text_form_field.dart';

class TextPropertyFormField<V> extends StatefulWidget {
  final PropertyController<String?, V> propertyController;
  final InputDecoration _decoration;
  final int? maxLines;
  final int? minLines;
  final TextAlignVertical? textAlignVertical;
  final bool expands;
  final bool obscureText;
  final bool autofocus;
  final TextStyle? style;
  final ErrorMessageMapper<V> _errorMessageMapper;
  final TextInputType? keyboardType;

  TextPropertyFormField({required this.propertyController,
    InputDecoration? decoration,
    this.maxLines = 1,
    this.minLines,
    this.textAlignVertical,
    this.expands = false,
    this.obscureText = false,
    this.autofocus = false,
    this.style,
    this.keyboardType,
    ErrorMessageMapper<V>? errorMessageMapper,
    super.key
  })
      : _decoration = decoration ?? InputDecoration(),
        _errorMessageMapper = errorMessageMapper ?? defaultErrorMessageMapper<V>;

  @override
  _TextPropertyFormFieldState createState() => _TextPropertyFormFieldState<V>();
}

class _TextPropertyFormFieldState<R> extends State<TextPropertyFormField<R>> {
  late TextEditingController _textEditingController;

  @override
  void initState() {
    super.initState();
    _textEditingController = TextEditingController(text: widget.propertyController.value);
    _textEditingController.addListener(() {
      widget.propertyController.value = _textEditingController.text;
    });
  }

  @override
  void dispose() {
    _textEditingController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<String?>(
        stream: widget.propertyController.validationMsg
            .distinct()
            .map((errorMsg) => widget._errorMessageMapper(context, errorMsg)),
        builder: (context, errorMsgSnapshot) {
          return TextField(
              keyboardType: widget.keyboardType,
              obscureText: widget.obscureText,
              textAlignVertical: widget.textAlignVertical,
              expands: widget.expands,
              maxLines: widget.maxLines,
              minLines: widget.minLines,
              autofocus: widget.autofocus,
              style: widget.style,
              autocorrect: false,
              controller: _textEditingController,
              decoration: widget._decoration.copyWith(errorText: errorMsgSnapshot.data));
        });
  }
}
