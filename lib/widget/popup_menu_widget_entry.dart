import 'package:flutter/material.dart';

const double _kMenuTextEntryHeight = 40.0;

class PopupMenuWidgetEntry<T> extends PopupMenuEntry<T> {
  final Widget content;

  const PopupMenuWidgetEntry({super.key, this.height = _kMenuTextEntryHeight, required this.content});

  @override
  final double height;

  @override
  bool represents(T? value) => false;

  @override
  State<PopupMenuWidgetEntry> createState() => _PopupMenuWidgetEntryState();
}

class _PopupMenuWidgetEntryState extends State<PopupMenuWidgetEntry> {
  @override
  Widget build(BuildContext context) => widget.content;
}
